import os

DEBUG = os.getenv("DEBUG", False)

if DEBUG:
    from .debug import *
else:
    from .prod import *

SUPPORTING_EXTENSIONS = [".crw", ".cr2", ".cr3", ".dng", ".raw"]

OUTPUT_DIR_NAME = "output"
OUTPUT_ARCHIVE_EXT = ".zip"
