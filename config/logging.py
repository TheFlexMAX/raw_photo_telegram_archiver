import logging
import os

DEBUG = os.getenv("DEBUG", False)

log_format = "%(asctime)s - %(name)s - %(levelname)s - %(module)s: %(message)s"

logging.basicConfig(
    filename="app.log",
    filemode="a",
    format=log_format,
    datefmt="%Y-%m-%d %H:%M:%S",
)

logger = logging.getLogger("AppLogger")

if DEBUG:
    logger.setLevel(logging.DEBUG)
else:
    logger.setLevel(logging.INFO)
