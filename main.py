import sys
from datetime import datetime
from multiprocessing import set_start_method, freeze_support
from os import chdir
from pathlib import Path

from app.models.archiver import Archiver
# from app.controllers.app_controller import AppController
from app.ui.main_view import MainView
from config.logging import logger


def set_application_path():
    if getattr(sys, 'frozen', False):
        application_path = Path(sys.executable).parent
    else:
        application_path = Path(__file__).resolve().parent

    chdir(application_path)


def main() -> None:
    logger.info('Application started.')

    freeze_support()
    set_start_method('spawn')
    set_application_path()

    time_start = datetime.now()
    print('Application started!')

    archiver = Archiver()
    main_view = MainView(archiver)
    # AppController(archiver, main_view)
    main_view.archiver = archiver

    main_view.mainloop()

    # app = MainView()
    #
    # app.archiver = Archiver()
    # app.archiver.run_gui(app)
    # app.update_frames()
    #
    # app.mainloop()

    print(f"\nCompleted for: {datetime.now() - time_start}")
    logger.info(f"Completed for: {datetime.now() - time_start}")

    # input("Press any key to exit.")


if __name__ == '__main__':
    main()
