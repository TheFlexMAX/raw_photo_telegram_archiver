#!/bin/bash

set -e

# WINDOWS BUILDER
#brew install --cask wine-stable
#
#curl -L "https://www.python.org/ftp/python/3.12.2/python-3.12.2-amd64.exe" -o python-installer.exe
#
#wine python-installer.exe /quiet InstallAllUsers=1 PrependPath=1
#
#wine python.exe -m pip install -r requirements/build.txt
#
#temp_build_dir="$(mktemp -d)"
#temp_dist_dir="$(mktemp -d)"
#
#wine python.exe -m PyInstaller --noconfirm --onefile --add-data='resources/:resources' --icon=resources/icons/icon.ico --name TeleArch_x86_64 --distpath "$temp_dist_dir" --workpath "$temp_build_dir" main.py
#
#mkdir -p binaries/win
#mv "$temp_dist_dir/TeleArch_x86_64.exe" binaries/win/TeleArch_x86_64.exe
#
#rm python-installer.exe
#
#rm -rf "$temp_build_dir"
#rm -rf "$temp_dist_dir"
#rm TeleArch_x86_64.spec


# MACOS arm64 builder
python3 -m venv build_macos_venv
source build_macos_venv/bin/activate
pip install -r requirements/build.txt

temp_build_dir="$(mktemp -d)"
temp_dist_dir="$(mktemp -d)"

pyinstaller --noconfirm --onefile --target-architecture=arm64 --add-data='resources/:resources' --icon=resources/icons/icon.icns --name TeleArch_arm64 --distpath "$temp_dist_dir" --workpath "$temp_build_dir" main.py

mkdir -p binaries/mac
mv "$temp_dist_dir/TeleArch_arm64" binaries/mac/TeleArch_arm64

rm -rf "$temp_build_dir"
rm -rf "$temp_dist_dir"

temp_build_dir="$(mktemp -d)"
temp_dist_dir="$(mktemp -d)"

# x86_64
pyinstaller --noconfirm --onefile --target-architecture=x86_64 --add-data='resources/:resources' --icon=resources/icons/icon.icns --name TeleArch_x86_64 --distpath "$temp_dist_dir" --workpath "$temp_build_dir" main.py

mv "$temp_dist_dir/TeleArch_x86_64" binaries/mac/TeleArch_x86_64

rm -rf "$temp_build_dir"
rm -rf "$temp_dist_dir"

deactivate

rm -rf __pycache__
rm TeleArch_x86_64.spec
rm TeleArch_arm64.spec
rm -rf build_macos_venv
