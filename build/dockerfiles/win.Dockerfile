FROM tobix/pywine:3.12

# Установите необходимые инструменты
USER root
RUN apt-get update && apt-get install -y --no-install-recommends \
    wget \
    xvfb

ARG WINEPREFFIX="/opt/mkuserwineprefix"
ARG PYTHONPATH="/opt/mkuserwineprefix/drive_c/Python/python.exe"

#RUN useradd -m wine
# Переключение на пользователя wine
#USER wine

RUN . "$WINEPREFFIX"

# Установка Python 3.12.2
# Поскольку прямая установка может быть сложной, вы можете скачать инсталлятор Python и запустить его через Wine
#RUN wget https://www.python.org/ftp/python/3.12.2/python-3.12.2-amd64.exe -O /home/wine/python-installer.exe
#RUN xvfb-run wine /home/wine/python-installer.exe /quiet InstallAllUsers=1 PrependPath=1

# Убедитесь, что Wine окончательно настроен
#RUN wine wineboot && wineserver -w

# Установите PyInstaller и зависимости проекта
# Обратите внимание, что вам может потребоваться настроить путь к Python в Wine
RUN dir /opt/wineprefix/drive_c/Python/ -s
RUN dir /opt/wineprefix/drive_c/Python/ -s

RUN wine cmd /c dir

RUN wine /opt/mkuserwineprefix/drive_c/Python/python.exe -v -m ensurepip
RUN wine "$PYTHONPATH" -m pip install --upgrade pip
RUN wine python -m pip install pyinstaller

RUN wine python -v -m ensurepip && \
    wine python -m pip install --upgrade pip && \
    wine python -m pip install pyinstaller

# Копируйте ваш проект внутрь контейнера и устанавливайте зависимости
COPY . /home/wine/project
WORKDIR /home/wine/project
RUN wine python -m pip install -r requirements.txt

# Соберите ваш проект с помощью PyInstaller
# Замените `your_script.py` на имя вашего скрипта
RUN xvfb-run wine pyinstaller --onefile your_script.py

# Запустите собранный исполняемый файл (пример)
CMD ["wine", "/home/wine/project/dist/your_script.exe"]


#FROM debian:buster
#
## Установите необходимые зависимости для Wine и добавления новых репозиториев
#RUN dpkg --add-architecture i386 && \
#    apt-get update && \
#    apt-get install -y wget gnupg2 software-properties-common apt-transport-https xvfb winbind
#
## Добавьте репозиторий Wine
#RUN wget -O- -q https://dl.winehq.org/wine-builds/winehq.key | apt-key add - && \
#    echo "deb https://dl.winehq.org/wine-builds/debian/ buster main" > /etc/apt/sources.list.d/winehq.list
#
## Установите Wine
#RUN apt-get update && \
#    apt-get install -y --install-recommends winehq-stable
#
## Установите Winetricks
#RUN wget https://raw.githubusercontent.com/Winetricks/winetricks/master/src/winetricks && \
#    chmod +x winetricks && \
#    mv winetricks /usr/local/bin
#
## Скачивание Python 3.12.2 и установка
#RUN wget https://www.python.org/ftp/python/3.12.2/python-3.12.2-amd64.exe -O /tmp/python-installer.exe
#RUN xvfb-run wine /tmp/python-installer.exe /quiet InstallAllUsers=1 PrependPath=1 \
#    && wineserver -w
#
## Настройка рабочего окружения Wine
#ENV WINEPREFIX /root/.wine
#ENV WINEARCH win64
#RUN wine wineboot && wineserver -w
#
## Копирование вашего проекта и сценария сборки в образ
#COPY . /app
#WORKDIR /app
#
## Установка PyInstaller и зависимостей проекта
#RUN wine pip install pyinstaller && \
#    wine pip install -r requirements.txt
#
## Сборка проекта с помощью PyInstaller
## Замените `your_script.py` на имя вашего основного скрипта Python
#RUN wine pyinstaller --onefile your_script.py
#
## Инструкции по запуску и работе с собранным проектом
#CMD ["wine", "/app/dist/your_script.exe"]


#FROM amake/x11client:buster
#
#ENV DEBIAN_FRONTEND noninteractive
#
## Inspired by monokrome/wine
#ENV WINE_MONO_VERSION 4.9.4
#USER root
#
## Install some tools required for creating the image
#RUN apt-get update \
#    && apt-get install -y --no-install-recommends \
#        curl \
#        unzip \
#        ca-certificates \
#        wget \
#        software-properties-common \
#        apt-transport-https \
#        gnupg2
#
## Install wine and related packages
#RUN dpkg --add-architecture i386 \
#        && apt-get update \
#        && apt-get install -y --no-install-recommends \
#                wine \
#                wine32 \
#                wine64 \
#        && rm -rf /var/lib/apt/lists/* \
#        && curl -SL 'https://raw.githubusercontent.com/Winetricks/winetricks/master/src/winetricks' -o /usr/local/bin/winetricks \
#        && chmod +x /usr/local/bin/winetricks
#
## Use the latest version of winetricks
#RUN curl -SL 'https://raw.githubusercontent.com/Winetricks/winetricks/master/src/winetricks' -o /usr/local/bin/winetricks \
#        && chmod +x /usr/local/bin/winetricks
#
## Get latest version of mono for wine
#RUN mkdir -p /usr/share/wine/mono \
#    && curl -SL 'https://sourceforge.net/projects/wine/files/Wine%20Mono/$WINE_MONO_VERSION/wine-mono-$WINE_MONO_VERSION.msi/download' -o /usr/share/wine/mono/wine-mono-$WINE_MONO_VERSION.msi \
#    && chmod +x /usr/share/wine/mono/wine-mono-$WINE_MONO_VERSION.msi
#
## Wine really doesn't like to be run as root, so let's use a non-root user
#USER xclient
#ENV HOME /home/xclient
#ENV WINEARCH win64
#ENV WINEPREFIX /home/xclient/.wine64
#
## Use xclient's home dir as working dir
#WORKDIR /home/xclient
#
#RUN rm -rf /home/xclient/.wine64 && wine64 wineboot
#
## Installing Python for wine
#RUN curl -o python-installer.exe https://www.python.org/ftp/python/3.12.2/python-3.12.2-amd64.exe
#
#RUN wine wineboot && wine msiexec /i python-installer.exe /qn
#RUN wine msiexec python -m pip install -r requirements/build.txt
#
#RUN echo "alias winegui='wine explorer /desktop=DockerDesktop,1024x768'" > ~/.bash_aliases
#
#COPY . /home/xclient/project
#WORKDIR /home/xclient/project

