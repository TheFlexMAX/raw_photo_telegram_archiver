from PIL import Image
from customtkinter import CTkFrame, CTkLabel, CTkButton, CTkImage

from app.ui.consts import MAIN_BG_COLOR, HEADER_1, HEADER_3, CORNER_RADIUS


class DoneFrame(CTkFrame):
    def __init__(self, master, show_frame_func, controller, **kwargs):
        super().__init__(master, **kwargs)
        self.controller = controller

        self.show_frame_func = show_frame_func
        self.configure(fg_color=MAIN_BG_COLOR)

        self.create_widgets()

    def create_widgets(self) -> None:
        # Создаем фрейм и упаковываем его
        frame = CTkFrame(self, fg_color=MAIN_BG_COLOR)
        frame.pack(padx=20, pady=20, fill='both', expand=True)

        center_frame = CTkFrame(frame, fg_color=MAIN_BG_COLOR)
        center_frame.pack(expand=True)

        top_frame = CTkFrame(center_frame, fg_color=MAIN_BG_COLOR)
        top_frame.pack()
        bottom_frame = CTkFrame(center_frame, fg_color=MAIN_BG_COLOR)
        bottom_frame.pack()

        label_success = CTkLabel(master=top_frame, text='Successfully created!', font=HEADER_3)
        label_success.pack(side='left', pady=(20, 10), fill='x')

        success_image = Image.open('./assets/icons/check.png')
        success_image = success_image.resize((40, 40), Image.ANTIALIAS)
        success_img = CTkImage(success_image, size=(30, 30))
        label_image = CTkLabel(master=top_frame, text='', image=success_img)
        label_image.image = success_img
        label_image.pack(side='left', pady=(20, 10))

        label_again = CTkLabel(master=bottom_frame, text='Again?', font=HEADER_1)
        label_again.pack(pady=(20, 10))

        refresh_image = Image.open('./assets/icons/round_arrow.png')
        refresh_image = refresh_image.resize((40, 40), Image.ANTIALIAS)
        refresh_img = CTkImage(refresh_image, size=(40, 40))

        self.button_again = CTkButton(master=bottom_frame, text='', image=refresh_img, compound='right',
                                 width=60, height=60, corner_radius=CORNER_RADIUS,
                                 command=lambda: print('Pressed Again!'))
        self.button_again.image = refresh_img
        self.button_again.pack(pady=(10, 0), )
