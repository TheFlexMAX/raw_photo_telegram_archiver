from os import name
from tkinter import Canvas

from PIL import Image
from customtkinter import CTkFrame, CTkScrollbar, CTkLabel, CTkImage

from app.ui.buttons.primary import H5PrimaryButton
from app.ui.buttons.secondary import H5SecondaryButton
from app.ui.consts import (
    CORNER_RADIUS,
    HEADER_5,
    HEADER_1,
    HEADER_6,
    SECONDARY_BUTTON_COLOR,
    MAIN_TEXT_COLOR,
    MAIN_BG_COLOR,
    MAIN_BG_COLOR_BG,
)


class BaseCheckoutDraw(CTkFrame):
    def _draw_top_frame(self) -> None:
        self.top_container = CTkFrame(self, fg_color=MAIN_BG_COLOR)
        self.top_container.pack(
            side='top', fill='both', expand=True, padx=65, pady=(30, 0)
        )

        self._draw_left_frame()
        self._draw_right_frame()

    def _draw_left_frame(self) -> None:
        self.files_frame = CTkFrame(self.top_container, fg_color=MAIN_BG_COLOR)
        self.files_frame.pack(side='left', fill='both', padx=(0, 20), expand=True)

        self.files_title_label = CTkLabel(self.files_frame, text='Files', font=HEADER_1)
        self.files_title_label.pack(pady=10)

        self.total_size_label_container = CTkFrame(
            self.files_frame, fg_color=MAIN_BG_COLOR
        )
        self.total_size_label_container.pack(side='top', fill='x', expand=False)

        self.total_size_label_container_text = CTkFrame(
            self.total_size_label_container, fg_color=MAIN_BG_COLOR
        )
        self.total_size_label_container_text.pack(side='top', fill='x', expand=False)

        self.total_size_label = CTkLabel(
            self.total_size_label_container_text, text='Total size', font=HEADER_5
        )
        self.total_size_label.pack(side='left', pady=2, fill='x')

        self.total_size_number_label = CTkLabel(
            self.total_size_label_container_text, text='28 GB', font=HEADER_5
        )
        self.total_size_number_label.pack(side='right', pady=2, fill='x')

        horizontal_line = CTkFrame(
            self.total_size_label_container, height=2, fg_color=MAIN_BG_COLOR_BG
        )
        horizontal_line.pack(side='bottom', fill='x', expand=True)

        # scrollable view zone
        self.files_canvas = Canvas(
            self.files_frame, bg=MAIN_BG_COLOR, highlightthickness=0
        )
        self.files_canvas.pack(side='left', fill='both', expand=True)

        self.files_scrollbar = CTkScrollbar(
            self.files_frame,
            command=self.files_canvas.yview,
            button_color=SECONDARY_BUTTON_COLOR,
        )
        self.files_scrollbar.pack(side='right', fill='y')

        self.files_scrollable_frame = CTkFrame(
            self.files_canvas, fg_color=MAIN_BG_COLOR
        )
        self.files_canvas.create_window(
            (0, 0), window=self.files_scrollable_frame, anchor='nw'
        )

    def _draw_middle_frame(self) -> None:
        self.divider_line = CTkFrame(
            self.top_container, width=2, fg_color=MAIN_TEXT_COLOR
        )
        self.divider_line.pack(fill='y', anchor='center', expand=True)

    def _draw_right_frame(self) -> None:
        self.archives_frame = CTkFrame(self.top_container, fg_color=MAIN_BG_COLOR)
        self.archives_frame.pack(side='right', fill='both', padx=(20, 0), expand=True)

        self.archives_title_label = CTkLabel(
            self.archives_frame, text='Archives', font=HEADER_1
        )
        self.archives_title_label.pack(pady=10)

        self.archives_title_label_container = CTkFrame(
            self.archives_frame, fg_color=MAIN_BG_COLOR
        )
        self.archives_title_label_container.pack(side='top', fill='x', expand=False)

        self.archives_title_label_container_text = CTkFrame(
            self.archives_title_label_container, fg_color=MAIN_BG_COLOR
        )
        self.archives_title_label_container_text.pack(
            side='top', fill='x', expand=False
        )

        self.amount_label = CTkLabel(
            self.archives_title_label_container_text, text='Amount', font=HEADER_5
        )
        self.amount_label.pack(side='left', pady=2, fill='x')

        self.amount_number_label = CTkLabel(
            self.archives_title_label_container_text, text='4 items', font=HEADER_5
        )
        self.amount_number_label.pack(side='right', pady=2, fill='x')

        horizontal_line = CTkFrame(
            self.archives_title_label_container, height=2, fg_color=MAIN_BG_COLOR_BG
        )
        horizontal_line.pack(side='bottom', fill='x', expand=True)

        # scrollable view zone
        self.archives_canvas = Canvas(
            self.archives_frame, bg=MAIN_BG_COLOR, highlightthickness=0
        )
        self.archives_canvas.pack(side='left', fill='both', expand=True)

        self.archives_scrollbar = CTkScrollbar(
            self.archives_frame,
            command=self.archives_canvas.yview,
            button_color=SECONDARY_BUTTON_COLOR,
        )
        self.archives_scrollbar.pack(side='right', fill='y')

        self.archives_scrollable_frame = CTkFrame(
            self.archives_canvas, fg_color=MAIN_BG_COLOR
        )
        self.archives_canvas.create_window(
            (0, 0), window=self.archives_scrollable_frame, anchor='nw'
        )

    def draw_file_list(self) -> None:
        file_icon_img = Image.open('./assets/icons/file.png')
        self.file_icon = CTkImage(
            light_image=file_icon_img, dark_image=file_icon_img, size=(15, 15)
        )

        for file in self.controller.archiver.file_handler.files:
            item_frame = CTkFrame(self.files_scrollable_frame, fg_color=MAIN_BG_COLOR)
            item_frame.pack(fill='x', padx=10, pady=3, anchor='nw')
            item_frame.bind('<MouseWheel>', self.file_canvas_on_mousewheel)

            label_icon = CTkLabel(
                item_frame, image=self.file_icon, compound='left', text=''
            )
            label_icon.pack(side='left', padx=5, pady=(5, 0), anchor='n', fill='x')
            label_icon.bind('<MouseWheel>', self.file_canvas_on_mousewheel)

            label = CTkLabel(
                item_frame, text=file.name, compound='left', font=HEADER_6
            )
            label.pack(side='left', padx=5, pady=(5, 0), anchor='n', fill='x')
            label.bind('<MouseWheel>', self.file_canvas_on_mousewheel)

        self.files_canvas.bind('<MouseWheel>', self.file_canvas_on_mousewheel)

        self.files_scrollable_frame.bind(
            '<Configure>', self.files_canvas_on_frame_configure
        )
        self.files_scrollable_frame.bind('<MouseWheel>', self.file_canvas_on_mousewheel)

        self.files_canvas.configure(yscrollcommand=self.files_scrollbar.set)

    def draw_archives_list(self) -> None:
        archive_icon_img = Image.open('./assets/icons/archive.png')
        self.archive_icon = CTkImage(
            light_image=archive_icon_img, dark_image=archive_icon_img, size=(18, 15)
        )

        for named_archive in self.controller.archiver.generate_named_archives(self.controller.archiver.get_archives()):
            item_frame = CTkFrame(
                self.archives_scrollable_frame, fg_color=MAIN_BG_COLOR
            )
            item_frame.pack(fill='x', padx=10, pady=3, anchor='nw')
            item_frame.bind('<MouseWheel>', self.archives_canvas_on_mousewheel)

            label_icon = CTkLabel(
                item_frame, image=self.archive_icon, compound='left', text=''
            )
            label_icon.pack(side='left', padx=5, pady=(5, 0), anchor='n', fill='x')
            label_icon.bind('<MouseWheel>', self.archives_canvas_on_mousewheel)

            label = CTkLabel(
                item_frame, text=named_archive[1], compound='left', font=HEADER_6
            )
            label.pack(side='left', padx=5, pady=(5, 0), anchor='n', fill='x')
            label.bind('<MouseWheel>', self.archives_canvas_on_mousewheel)

        self.archives_canvas.bind('<MouseWheel>', self.archives_canvas_on_mousewheel)

        self.archives_scrollable_frame.bind(
            '<Configure>', self.archives_canvas_on_frame_configure
        )
        self.archives_scrollable_frame.bind(
            '<MouseWheel>', self.archives_canvas_on_mousewheel
        )

        self.archives_canvas.configure(yscrollcommand=self.archives_scrollbar.set)

    def _draw_footer_frame(self):
        self.footer_frame = CTkFrame(self, fg_color=MAIN_BG_COLOR)
        self.footer_frame.pack(side='bottom', fill='x', padx=65, pady=(0, 45))

        self.footer_back_button_container = CTkFrame(
            self.footer_frame, fg_color=MAIN_BG_COLOR
        )
        self.footer_back_button_container.pack(
            side='left', fill='x', padx=(0, 25), expand=True
        )

        self.footer_confirm_button_container = CTkFrame(
            self.footer_frame, fg_color=MAIN_BG_COLOR
        )
        self.footer_confirm_button_container.pack(
            side='right', fill='x', padx=(25, 0), expand=True
        )

        self.back_button = H5SecondaryButton(
            self.footer_back_button_container,
            text='Go back',
            # command=self.go_back_click,
        )
        self.back_button.pack(side='top', anchor='center', pady=10)

        self.confirm_button = H5PrimaryButton(
            self.footer_confirm_button_container,
            text='Confirm',
            # command=self.confirm_click,
        )
        self.confirm_button.pack(side='top', anchor='center', pady=10)

    def file_canvas_on_mousewheel(self, event):
        if name == 'nt':  # For Windows
            self.files_canvas.yview_scroll(int(-1 * (event.delta / 120)), 'units')
        elif name == 'Darwin' or name == 'posix':  # For MacOS
            self.files_canvas.yview_scroll(int(-1 * event.delta), 'units')
        else:  # For Linux
            self.files_canvas.yview_scroll(int(-1 * (event.delta / 120)), 'units')

    def archives_canvas_on_mousewheel(self, event):
        if name == 'nt':  # For Windows
            self.archives_canvas.yview_scroll(int(-1 * (event.delta / 120)), 'units')
        elif name == 'Darwin' or name == 'posix':  # For MacOS
            self.archives_canvas.yview_scroll(int(-1 * event.delta), 'units')
        else:  # For Linux
            self.archives_canvas.yview_scroll(int(-1 * (event.delta / 120)), 'units')

    def files_canvas_on_frame_configure(self, event):
        self.files_canvas.configure(scrollregion=self.files_canvas.bbox('all'))

    def archives_canvas_on_frame_configure(self, event):
        self.archives_canvas.configure(scrollregion=self.archives_canvas.bbox('all'))


class CheckoutFrame(BaseCheckoutDraw):
    def __init__(self, master, show_frame_func, controller, **kwargs):
        super().__init__(master, **kwargs)
        self.controller = controller
        self.archiver = None

        self.show_frame_func = show_frame_func
        self.configure(corner_radius=CORNER_RADIUS, fg_color=MAIN_BG_COLOR)

        self._draw_top_frame()
        self._draw_middle_frame()
        self._draw_footer_frame()

    def update_file_list_size(self) -> None:
        file_size_bytes = self.controller.archiver.file_handler.get_file_total_size()
        file_size_str = self.controller.archiver.file_handler.convert_size(file_size_bytes)
        self.total_size_number_label.configure(text=file_size_str)

    def update_archives_list_amount(self) -> None:
        self.amount_number_label.configure(text=f'{len(self.controller.archiver.get_archives())} items')

    def set_data(self, data) -> None:
        self.draw_file_list()
        self.update_file_list_size()
        self.draw_archives_list()
        self.update_archives_list_amount()
