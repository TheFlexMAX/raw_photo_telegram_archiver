from tkinter import Canvas
from tkinter.constants import CENTER

from PIL import Image
from customtkinter import CTkFrame, CTkLabel, CTkButton, CTkImage

from app.ui.buttons.primary import H6PrimaryButton
from app.ui.consts import (
    MAIN_BG_COLOR,
    MAIN_BG_COLOR_BG,
    MAIN_TEXT_COLOR,
    CORNER_RADIUS,
    HEADER_1,
    HEADER_6,
)


class WelcomeFrame(CTkFrame):
    def __init__(self, master, show_frame_func, controller, **kwargs):
        super().__init__(master, **kwargs)
        self.controller = controller
        self.archiver = master.archiver

        self.show_frame_func = show_frame_func
        self.configure(fg_color=MAIN_BG_COLOR)

        self.button_set_path = None
        self.menu_button = None

        self.create_widgets()

    def create_widgets(self) -> None:
        drop_area = Canvas(
            self, width=718, height=366, bg=MAIN_BG_COLOR, highlightthickness=0
        )
        drop_area.place(relx=0.5, rely=0.5, anchor=CENTER)

        lines_width = 15
        dash_params = (15, 15)
        left_line_coords = 0, 0, 0, 366
        drop_area.create_line(
            left_line_coords, dash=dash_params, width=lines_width, fill=MAIN_BG_COLOR_BG
        )
        top_line_coords = 0, 0, 718, 0
        drop_area.create_line(
            top_line_coords, dash=dash_params, width=lines_width, fill=MAIN_BG_COLOR_BG
        )
        right_line_coords = 718, 0, 718, 366
        drop_area.create_line(
            right_line_coords,
            dash=dash_params,
            width=lines_width,
            fill=MAIN_BG_COLOR_BG,
        )
        bottom_line_coords = 718, 366, 0, 366
        drop_area.create_line(
            bottom_line_coords,
            dash=dash_params,
            width=lines_width,
            fill=MAIN_BG_COLOR_BG,
        )

        dnd_icon_image = Image.open('./assets/icons/drag_and_drop.png')
        dnd_icon = CTkImage(light_image=dnd_icon_image, dark_image=dnd_icon_image, size=(100, 100))
        dnd_icon_label = CTkLabel(self, image=dnd_icon, text='')
        dnd_icon_label.place(anchor=CENTER, relx=0.5, rely=0.33)

        label_drag_drop = CTkLabel(
            drop_area,
            text='Drag & Drop',
            text_color=MAIN_TEXT_COLOR,
            font=HEADER_1,
        )
        label_drag_drop.place(relx=0.5, rely=0.5, anchor=CENTER)

        label_or = CTkLabel(
            self, text='or', text_color=MAIN_TEXT_COLOR, font=HEADER_6
        )
        label_or.place(relx=0.5, rely=0.6, anchor=CENTER)

        self.button_set_path = H6PrimaryButton(
            self,
            text='Set path',
            # command=self.set_path,
        )
        self.button_set_path.place(relx=0.5, rely=0.7, anchor=CENTER)

        self.menu_button = CTkButton(
            self,
            text='≡',
            fg_color=MAIN_BG_COLOR,
            width=35,
            height=25,
            corner_radius=CORNER_RADIUS,
            # command=self.open_menu,
        )
        self.menu_button.place(relx=0.95, rely=0.05, anchor=CENTER)
