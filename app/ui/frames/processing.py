from queue import Empty

from customtkinter import CTkFrame, CTkLabel, CTkProgressBar

from app.helpers import format_duration
from app.ui.consts import (
    MAIN_BG_COLOR,
    HEADER_1,
    HEADER_6,
    NAV_ELEMENTS_COLOR,
    MAIN_BG_COLOR_BG,
)


class ProcessingFrame(CTkFrame):
    def __init__(self, master, show_frame_func, controller, **kwargs):
        """
        Initializes the processing frame.

        Args:
            master: The master widget.
            show_frame_func: The function to show a specific frame.
            controller: The controller object.
            **kwargs: Additional keyword arguments.

        Returns:
            None
        """
        super().__init__(master, **kwargs)
        self.process = None
        self.controller = controller

        self.show_frame_func = show_frame_func
        self.configure(fg_color=MAIN_BG_COLOR)

        self.create_widgets()

    def create_widgets(self) -> None:
        """
        Creates and configures the widgets for the processing frame.

        Args:
            self: The instance of the class.

        Returns:
            None
        """
        main_frame = CTkFrame(self, fg_color=MAIN_BG_COLOR, width=100, height=200)
        main_frame.pack(
            padx=round(self.master.winfo_width() / 4), fill='x', expand=True
        )

        # Archiving label
        archiving_label = CTkLabel(main_frame, text='Archiving...', font=HEADER_1)
        archiving_label.pack()

        # Progress bar
        self.progress_bar = CTkProgressBar(
            main_frame,
            height=8,
            progress_color=NAV_ELEMENTS_COLOR,
            fg_color=MAIN_BG_COLOR_BG,
            corner_radius=2,
        )
        self.progress_bar.set(0)
        self.progress_bar.pack(pady=(20, 5), fill='x')

        # Remaining time label
        remaining_time_label = CTkLabel(
            main_frame, text='Remaining time', font=HEADER_6
        )
        remaining_time_label.pack(side='left', fill='x')

        # Time label
        self.time_label = CTkLabel(main_frame, text='00:00:12', font=HEADER_6)
        self.time_label.pack(side='right', fill='x')

    def update_progress_bar(self):
        try:
            progress_data = self.controller.percentage_queue.get()
            percentage = progress_data.get('percentage') / 100
            self.progress_bar.set(percentage)
            self.time_label.configure(text=f"{format_duration(progress_data.get('remaining_time_and_speed')[0])}")
            if percentage < 1:
                self.after(100, self.update_progress_bar)
            else:
                self.controller.view.show_frame_func('DoneFrame')
        except Empty:
            self.after(100, self.update_progress_bar)

    def set_data(self, data) -> None:
        self.controller.run_process()
