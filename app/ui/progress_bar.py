from os import system, name
from time import time


class ProgressBar:
    """
    A class to represent a progress bar for tracking the progress of a task.

    Attributes:
        total_steps (int): The total number of steps in the task.
        current_step (int): The current step in the task.
        start_time (float): The start time of the task.
    """

    def __init__(self, total_steps: int) -> None:
        """
        Initializes a ProgressBar object with the total number of steps.

        Args:
            total_steps (int): The total number of steps in the task.
        """
        self.total_steps = total_steps
        self.current_step = 0
        self.start_time = time()

    def move_cursor_up(self, lines=1) -> None:
        """
        Moves the terminal cursor up by the specified number of lines.

        Args:
            lines (int, optional): The number of lines to move the cursor up. Defaults to 1.
        """
        print(f"\033[{lines}A", end='')

    def clean_console(self) -> None:
        """
        Clears the console screen.

        Args:
            self: The instance of the class.

        Returns:
            None.

        Raises:
            None.
        """
        system('cls' if name == 'nt' else 'clear')

    def print_progress(self, percent_complete, remaining_time) -> None:
        """
        Prints the progress bar with the percentage complete and remaining time.

        Args:
            percent_complete (float): The percentage of the task completed.
            remaining_time (float): The estimated remaining time for the task to complete.
        """
        progress_bar = ('#' * int(percent_complete // 2)).ljust(50)
        progress_bar_str = f"\r[{progress_bar}] {percent_complete:.2f}% completed, remaining: {remaining_time:.2f} sec."
        print(f"{progress_bar_str}")

    def print_info(self, speed) -> None:
        """
        Prints additional information about the progress of the task.

        Args:
            speed (float): The speed of the task in units per second.
        """
        info_str = f"{self.current_step}/{self.total_steps} files archived at {speed:.2f} files/sec"
        print(f"{info_str}")

    def get_remaining_time_and_speed(self):
        elapsed_time = time() - self.start_time
        speed = self.current_step / elapsed_time if elapsed_time > 0 else 0
        remaining_time = (elapsed_time / self.current_step) * (
                self.total_steps - self.current_step
        )
        return remaining_time, speed

    def update(self, step_increment=1) -> None:
        """Updates the progress of the task.

        Args:
            step_increment (int, optional): The number of steps to increment the progress. Defaults to 1.
        """
        self.current_step += step_increment
        # percent_complete = (self.current_step / self.total_steps) * 100

        # elapsed_time = time() - self.start_time
        # speed = self.current_step / elapsed_time if elapsed_time > 0 else 0
        # remaining_time = (elapsed_time / self.current_step) * (
        #         self.total_steps - self.current_step
        # )

        # self.clean_console()
        print('\033[2K', end='')
        # self.print_progress(percent_complete, remaining_time)
        print('\033[A\033[2K', end='')
        # self.print_info(speed)

        if self.current_step >= 1:
            self.move_cursor_up(2)
        elif self.current_step == self.total_steps:
            print('\nDone')
