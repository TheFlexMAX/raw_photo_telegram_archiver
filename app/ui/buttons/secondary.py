from typing import Any

from app.ui.buttons.base import BaseButton
from app.ui.consts import (
    HEADER_1,
    HEADER_2,
    HEADER_3,
    HEADER_4,
    HEADER_5,
    HEADER_6,
    SECONDARY_BUTTON_TEXT_COLOR,
    SECONDARY_BUTTON_COLOR,
)


class HBaseSecondaryButton(BaseButton):
    def __init__(self, master: Any, *args, **kwargs) -> None:
        super().__init__(
            master,
            text_color=SECONDARY_BUTTON_TEXT_COLOR,
            fg_color=SECONDARY_BUTTON_COLOR,
            *args,
            **kwargs
        )


class H1SecondaryButton(HBaseSecondaryButton):
    def __init__(self, master: Any, *args, **kwargs) -> None:
        super().__init__(master, font=HEADER_1, *args, **kwargs)


class H2SecondaryButton(HBaseSecondaryButton):
    def __init__(self, master: Any, *args, **kwargs) -> None:
        super().__init__(master, font=HEADER_2, *args, **kwargs)


class H3SecondaryButton(HBaseSecondaryButton):
    def __init__(self, master: Any, *args, **kwargs) -> None:
        super().__init__(master, font=HEADER_3, *args, **kwargs)


class H4SecondaryButton(HBaseSecondaryButton):
    def __init__(self, master: Any, *args, **kwargs) -> None:
        super().__init__(master, font=HEADER_4, *args, **kwargs)


class H5SecondaryButton(HBaseSecondaryButton):
    def __init__(self, master: Any, *args, **kwargs) -> None:
        super().__init__(master, font=HEADER_5, *args, **kwargs)


class H6SecondaryButton(HBaseSecondaryButton):
    def __init__(self, master: Any, *args, **kwargs) -> None:
        super().__init__(master, font=HEADER_6, *args, **kwargs)
