from typing import Any

from app.ui.buttons.base import BaseButton
from app.ui.consts import (
    HEADER_1,
    HEADER_2,
    HEADER_3,
    HEADER_4,
    HEADER_5,
    HEADER_6,
    MAIN_TEXT_COLOR,
)


class HBasePrimaryButton(BaseButton):
    def __init__(self, master: Any, *args, **kwargs) -> None:
        super().__init__(master, text_color=MAIN_TEXT_COLOR, *args, **kwargs)


class H1PrimaryButton(BaseButton):
    def __init__(self, master: Any, *args, **kwargs):
        super().__init__(master, font=HEADER_1, *args, **kwargs)


class H2PrimaryButton(BaseButton):
    def __init__(self, master: Any, *args, **kwargs):
        super().__init__(master, font=HEADER_2, *args, **kwargs)


class H3PrimaryButton(BaseButton):
    def __init__(self, master: Any, *args, **kwargs):
        super().__init__(master, font=HEADER_3, *args, **kwargs)


class H4PrimaryButton(BaseButton):
    def __init__(self, master: Any, *args, **kwargs):
        super().__init__(master, font=HEADER_4, *args, **kwargs)


class H5PrimaryButton(BaseButton):
    def __init__(self, master: Any, *args, **kwargs):
        super().__init__(master, font=HEADER_5, *args, **kwargs)


class H6PrimaryButton(BaseButton):
    def __init__(self, master: Any, *args, **kwargs):
        super().__init__(master, font=HEADER_6, *args, **kwargs)
