from typing import Any

from customtkinter import CTkButton

from app.ui.consts import CORNER_RADIUS


class BaseButton(CTkButton):
    def __init__(self, master: Any, *args, **kwargs):
        super().__init__(master, width=100, height=25, corner_radius=CORNER_RADIUS, *args, **kwargs)
