from customtkinter import CTk

from app.controllers.checkout import CheckoutController
from app.controllers.done import DoneController
from app.controllers.processing import ProcessingController
from app.controllers.welcome import WelcomeController
from app.ui.consts import MAIN_BG_COLOR
from app.ui.frames.checkout import CheckoutFrame
from app.ui.frames.done import DoneFrame
from app.ui.frames.processing import ProcessingFrame
from app.ui.frames.welcome import WelcomeFrame


class MainView(CTk):
    def __init__(self, archiver=None) -> None:
        super().__init__()
        self.archiver = archiver

        self.geometry('854x480')
        self.minsize(854, 480)
        self.configure(bg_color=MAIN_BG_COLOR)
        self.title('PTA')
        self.resizable(True, True)

        # self.protocol('WM_DELETE_WINDOW', self.on_close)

        self.frames = {}

        welcome_frame = WelcomeFrame(self, self.show_frame, None)
        checkout_frame = CheckoutFrame(self, self.show_frame, None)
        processing_frame = ProcessingFrame(self, self.show_frame, None)
        done_frame = DoneFrame(self, self.show_frame, None)

        welcome_controller = WelcomeController(welcome_frame, self.archiver)
        checkout_controller = CheckoutController(checkout_frame, self.archiver)
        self.processing_controller = ProcessingController(processing_frame, self.archiver)
        done_controller = DoneController(done_frame, self.archiver)

        welcome_frame.controller = welcome_controller
        checkout_frame.controller = checkout_controller
        processing_frame.controller = self.processing_controller
        done_frame.controller = done_controller

        self.frames = {
            'WelcomeFrame': welcome_frame,
            'CheckoutFrame': checkout_frame,
            'ProcessingFrame': processing_frame,
            'DoneFrame': done_frame,
        }

        self.show_frame('WelcomeFrame')

    def update_frames(self):
        for F in self.frames.values():
            F.archiver = self.archiver

    def hide_all_frames(self) -> None:
        for frame in self.frames.values():
            frame.pack_forget()

    def show_frame(self, page_frame: str, data=None) -> None:
        self.hide_all_frames()

        frame = self.frames[page_frame]
        if data:
            frame.set_data(data)
        frame.pack(fill='both', expand=True)
        frame.tkraise()

    def on_close(self, event) -> None:
        """
        Handle the close event.

        If the main view has a processing controller attribute, stop the process. Then, destroy the main view.

        Args:
            event: The close event.

        Returns:
            None
        """
        if hasattr(self, 'processing_controller'):
            self.processing_controller.stop_process()

        self.destroy()
