from enum import Enum

MAIN_BG_COLOR = '#04080F'
MAIN_BG_COLOR_BG = '#0B1320'
PRIMARY_BUTTON_COLOR = '#507DBC'
SECONDARY_BUTTON_COLOR = '#0B1320'
SECONDARY_BUTTON_TEXT_COLOR = '#A1C6EA'
MAIN_TEXT_COLOR = '#DAE3E5'
NAV_ELEMENTS_COLOR = '#A1C6EA'
CORNER_RADIUS = 2

MAIN_FONT = 'Roboto medium'
HEADER_1 = (MAIN_FONT, 32)
HEADER_2 = (MAIN_FONT, 24)
HEADER_3 = (MAIN_FONT, 20)
HEADER_4 = (MAIN_FONT, 16)
HEADER_5 = (MAIN_FONT, 14)
HEADER_6 = (MAIN_FONT, 11)


class MouseEvents(Enum):
    LEFT_CLICK = '<Button-1>'
    MIDDLE_CLICK = '<Button-2>'
    RIGHT_CLICK = '<Button-3>'
    LEFT_CLICK_RELEASE = '<ButtonRelease-1>'
    MOUSE_MOVE = '<Motion>'
    ENTER_WIDGET = '<Enter>'
    LEAVE_WIDGET = '<Leave>'


class KeyboardEvents(Enum):
    KEY_PRESS = '<KeyPress>'
    KEY_RELEASE = '<KeyRelease>'


class OtherEvents(Enum):
    FOCUS_IN = '<FocusIn>'
    FOCUS_OUT = '<FocusOut>'
    CLOSE_WINDOW = '<Destroy>'
