from app.ui.consts import MouseEvents


class DoneController:
    def __init__(self, view, archiver):
        self.view = view
        self.archiver = archiver

        self.setup_bindings()

    def setup_bindings(self) -> None:
        self.view.button_again.bind(MouseEvents.LEFT_CLICK.value, self.on_repeat_click)

    def on_repeat_click(self, event) -> None:
        self.view.show_frame_func('WelcomeFrame')
