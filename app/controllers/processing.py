import multiprocessing as mp


class ProcessingController:
    def __init__(self, view, archiver):
        self.view = view
        self.archiver = archiver

        self.process = None
        self.progress_queue = mp.Queue()
        self.percentage_queue = mp.Queue()

    def run_process(self) -> None:
        self.archiver.init()
        self.process = mp.Process(target=self.archiver.process, args=(self.progress_queue, self.percentage_queue,))
        self.process.start()
        self.view.update_progress_bar()

    def stop_process(self) -> None:
        """
        Stops the current process if it exists.

        Args:
            self: The instance of the class.

        Returns:
            None
        """
        if self.process is not None:
            self.process.terminate()
            self.process.join()
            self.process = None
