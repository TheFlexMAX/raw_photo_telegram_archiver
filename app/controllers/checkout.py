from app.ui.consts import MouseEvents


class CheckoutController:
    def __init__(self, view, archiver):
        self.view = view
        self.archiver = archiver
        self.setup_bindings()

    def setup_bindings(self):
        self.view.back_button.bind(MouseEvents.LEFT_CLICK.value, self.on_go_back_click)
        self.view.confirm_button.bind(MouseEvents.LEFT_CLICK.value, self.on_confirm_click)

    def on_go_back_click(self, event):
        self.view.show_frame_func('WelcomeFrame')

    def on_confirm_click(self, event):
        self.view.show_frame_func('ProcessingFrame', {'process': 'run'})
