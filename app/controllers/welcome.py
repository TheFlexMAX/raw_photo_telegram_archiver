from pathlib import Path
from tkinter import filedialog

from app.ui.consts import MouseEvents


class WelcomeController:
    def __init__(self, view, archiver):
        self.view = view
        self.archiver = archiver
        self.setup_bindings()

    def setup_bindings(self):
        self.view.button_set_path.bind(MouseEvents.LEFT_CLICK.value, self.set_path)
        self.view.menu_button.bind(MouseEvents.LEFT_CLICK.value, self.open_menu)

    def open_menu(self, event):
        # Здесь можно разместить логику для открытия меню
        pass

    def set_path(self, event) -> None:
        path = filedialog.askdirectory()
        print(f"Selected path: {path}")
        if not path:
            return

        self.archiver.directory = Path(path)
        self.archiver.init()
        self.view.pack_forget()
        self.view.show_frame_func('CheckoutFrame', data={'path': path})

    def set_data(self, data) -> None:
        pass
