from datetime import datetime
from multiprocessing import current_process, Queue
from pathlib import Path, WindowsPath
from shutil import rmtree
from typing import List
from zipfile import ZipFile, ZIP_DEFLATED

from app.types.models import Archive, File
from app.ui.progress_bar import ProgressBar
from config.logging import logger


class ArchiveManager:
    """
    A class to manage the creation of archives from chunks of files.

    Attributes:
        output_dir (Path): The directory where the archives will be created.
        size_limit (int): The size limit for each archive.
        current_size (int): The current size of the files in the current chunk.
        current_chunk (List[WindowsPath]): The list of files in the current chunk.
    """

    def __init__(self, output_dir: Path, size_limit: int) -> None:
        """
        Initializes an ArchiveManager object with the output directory and size limit.

        Args:
            output_dir (Path): The directory where the archives will be created.
            size_limit (int): The size limit for each archive.
        """
        self.output_dir = output_dir
        self.size_limit = size_limit
        self.current_size = 0
        self.current_chunk = []

        self.init_dirs()

    def create_archives(self, chunks_info_list, queue: Queue) -> None:
        for archive, archive_name in chunks_info_list:
            self.archive_chunk(archive, archive_name, queue)

    def archive_chunk(self, archive: Archive, archive_name: str, queue: Queue) -> None:
        """
        Archives a chunk of files into a single archive.

        Args:
            archive (List[WindowsPath]): The list of files to be archived.
            progress_bar (ProgressBar): The progress bar to update during archiving.
            archive_name (int): The number of the archive.
        """
        zip_file_path = self.output_dir / self.construct_archive_name(
            archive, archive_name
        )

        process = current_process()
        logger.info(f"Archiving {zip_file_path} in PID: {process.pid}")

        with ZipFile(zip_file_path, 'w', ZIP_DEFLATED) as zipf:
            for file in archive:
                zipf.write(file, file.relative_to(file.parent))
                queue.put(1)

        logger.info(f"Archived {zip_file_path} in PID: {process.pid}")

    def construct_archive_name(self, archive: Archive, archive_name: str) -> str:
        """
        Constructs the name for the archive based on the chunk of files and the archive number.

        Args:
            archive (List[WindowsPath]): The list of files in the chunk.
            archive_name (str): The name of the archive.

        Returns:
            str: The name of the archive.
        """
        return f"{self.human_readable_date(archive[0])}_{archive_name}"

    def human_readable_date(self, file: File) -> str:
        """
        Formats the creation date of the file into a human-readable date string.

        Args:
            file (File): The file to retrieve the creation date from.

        Returns:
            str: The human-readable date string.
        """
        return datetime.fromtimestamp(file.stat().st_ctime).strftime('%d_%m_%Y')

    def init_dirs(self) -> None:
        """Initializes the output directory by clearing its contents and creating it if necessary."""
        if self.output_dir.exists():
            for path in Path(self.output_dir).glob('**/*'):
                if path.is_file():
                    path.unlink()
                elif path.is_dir():
                    rmtree(path)
            logger.debug('Output directory removed')

        if not self.output_dir.exists():
            self.output_dir.mkdir(parents=True, exist_ok=True)
            logger.debug('Output directory created')
