import math
from pathlib import Path, WindowsPath
from typing import List


class FileHandler:
    """
    A class to handle files in a directory and find supported files based on extensions.

    Attributes:
        directory (Path): The directory containing the files to be handled.
        files (List[WindowsPath]): A list of files found in the directory.
    """

    def __init__(self, directory: Path) -> None:
        """
        Initializes a FileHandler object with the directory containing the files.

        Args:
            directory (Path): The directory containing the files to be handled.
        """
        self.directory = directory
        self.files = []

    def find_supported_files(self, extensions: List[str]) -> None:
        """
        Finds files with supported extensions in the directory.

        Args:
            extensions (List[str]): A list of supported file extensions.
        """
        files = [
            item
            for item in self.directory.iterdir()
            if item.is_file() and item.suffix.lower() in extensions
        ]
        self.files = sorted(files, key=lambda item: item.stat().st_mtime)

    def get_file_count(self) -> int:
        """
        Gets the number of files found in the directory.

        Returns:
            int: The number of files found in the directory.
        """
        return len(self.files)

    def get_file_total_size(self) -> int:
        return sum(file.stat().st_size for file in self.files)

    def convert_size(self, size_bytes) -> str:
        if size_bytes == 0:
            return '0B'
        size_name = ('B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB')
        i = int(math.floor(math.log(size_bytes, 1024)))
        p = math.pow(1024, i)
        s = round(size_bytes / p, 2)

        return f"{s} {size_name[i]}"
