import multiprocessing as mp
from multiprocessing import Queue
from pathlib import Path, WindowsPath
from typing import List

from app.helpers import find_empty_core, has_empty_cores, remove_empty_cores
from app.models.archive_manager import ArchiveManager
from app.models.file_handler import FileHandler
from app.types.models import Archive, NamedArchive
from app.ui.progress_bar import ProgressBar
from config.logging import logger
from config.settings import (
    OUTPUT_ARCHIVE_EXT,
    OUTPUT_DIR_NAME,
    SIZE_FOR_ARCHIVE,
    SUPPORTING_EXTENSIONS,
)

queue = None


class Archiver:
    """
    A class to represent an archiver for processing and archiving files.

    Attributes:
        directory (Path): The directory containing the files to be archived.
        file_handler (FileHandler): The file handler object for finding supported files.
        archive_manager (ArchiveManager): The archive manager object for archiving files.
        progress_bar (ProgressBar): The progress bar object for tracking the archiving progress.
    """

    # def __init__(self, directory: Path) -> None:
    #     self.directory = directory
    #     self.file_handler = FileHandler(directory)
    #     self.archive_manager = ArchiveManager(
    #         directory / OUTPUT_DIR_NAME, SIZE_FOR_ARCHIVE
    #     )
    #     self.progress_bar = None
    #     self.progress_queue = None

    def __init__(self) -> None:
        self.archive_manager = None
        self.progress_bar = None
        self.file_handler = None
        self.directory = None
        self.ui = None

        manager = mp.Manager()
        self.progress_queue = manager.Queue()
        self.percentage_queue = manager.Queue()

    def init(self) -> None:
        """
        Initializes the archiving process by finding supported files and initializing the progress bar.
        """
        self.file_handler = FileHandler(self.directory)
        self.file_handler.find_supported_files(SUPPORTING_EXTENSIONS)
        self.progress_bar = ProgressBar(self.file_handler.get_file_count())
        self.archive_manager = ArchiveManager(
            self.directory / OUTPUT_DIR_NAME, SIZE_FOR_ARCHIVE
        )
        logger.info('Application initialized.')

    def init_pool(self, progress_queue: Queue) -> None:
        global queue
        queue = progress_queue

    def get_archives(self):
        return self.split_to_archives(self.file_handler.files)

    def generate_named_archives(self, archives: List[Archive]) -> List[NamedArchive]:
        return [
            (archive, f"archive_{i:03}{OUTPUT_ARCHIVE_EXT}")
            for i, archive in enumerate(archives, start=1)
        ]

    def process(self, progress_queue, percentage_queue) -> None:
        """
        Processes and archives the files.
        """
        cpu_count = mp.cpu_count()
        archives = self.get_archives()

        logger.info(f"Planned amount archives: {len(archives)}")

        named_archives: List[NamedArchive] = self.generate_named_archives(archives)
        chunks_for_process = self.distribute_chunk_infos(named_archives, cpu_count)
        logger.info(f"Prepared data {chunks_for_process}")
        logger.info(f"Planned chunks {len(chunks_for_process)}")
        logger.info('Processing chunks...')

        process_count_to_create = len(chunks_for_process)
        if process_count_to_create == 0:
            return

        # manager = mp.Manager()
        # self.progress_queue = manager.Queue()
        total_files = self.file_handler.get_file_count()
        self.progress_bar = ProgressBar(total_files)
        with mp.Pool(
                process_count_to_create,
                initializer=self.init_pool,
                initargs=(self.progress_queue,),
        ) as pool:
            res = pool.starmap_async(
                self.archive_manager.create_archives,
                [(chunk, self.progress_queue) for chunk in chunks_for_process],
            )

            packed_files = 0
            while packed_files < total_files:
                update = self.progress_queue.get()
                logger.debug(f"Progress bar update info: {update}")
                logger.debug(f"packed files amount: {packed_files}")
                logger.debug(f"total files amount: {total_files}")
                if update:
                    packed_files += update
                    self.progress_bar.update(update)
                    # print(f'progress_queue.put(update) : {progress_queue.put(update)}')
                    percentage = self.calculate_progress_percentage(packed_files, total_files)
                    logger.debug(
                        f"'percentage': {percentage}, 'remaining_time_and_speed': {self.progress_bar.get_remaining_time_and_speed()}")
                    percentage_queue.put({'percentage': percentage,
                                          'remaining_time_and_speed': self.progress_bar.get_remaining_time_and_speed()})

            res.wait()

    def calculate_progress_percentage(self, packed_files: int, total_files: int) -> float:
        """
        Calculates the progress percentage based on the number of packed files and total files.

        Args:
            packed_files: The number of packed files.
            total_files: The total number of files.

        Returns:
            float: The progress percentage.
        """

        return 0 if total_files == 0 else (packed_files / total_files) * 100

    def run_gui(self, ui) -> None:
        self.ui = ui

    def split_to_archives(self, files: List[WindowsPath]) -> List[Archive]:
        """
        Splits the list of files into chunks based on the size limit for archiving.

        Args:
            files (List[WindowsPath]): The list of files to be split into chunks.

        Returns:
            List[List[WindowsPath]]: A list of chunks containing lists of files.
        """
        total_size = 0
        archives = []
        archive = []

        for f in files:
            f_size = f.stat().st_size
            if total_size + f_size <= SIZE_FOR_ARCHIVE:
                archive.append(f)
                total_size += f_size
            else:
                if archive:
                    archives.append(archive)
                archive = [f]
                total_size = f_size

        if archive:
            archives.append(archive)

        return archives

    def distribute_chunk_infos(
            self, chunk_infos: List[NamedArchive], cores_count: int
    ) -> List[List[NamedArchive]]:
        chunks_count = len(chunk_infos)

        chunks_per_core = max(1, chunks_count // cores_count)
        remainder = chunks_count % cores_count

        res = [[] for _ in range(cores_count)]

        for i, core in enumerate(res):
            start = i * chunks_per_core
            end = start + chunks_per_core
            core.extend(chunk_infos[start:end])

        if remainder > 0 and remainder != chunks_count:
            empty_core_idx = find_empty_core(res)
            if empty_core_idx is not None:
                res[empty_core_idx].extend(chunk_infos[-remainder:])
            else:
                core_idx = 0
                for chunk in chunk_infos[-remainder:]:
                    core_idx %= cores_count
                    res[core_idx].append(chunk)
                    core_idx += 1

        return remove_empty_cores(res) if has_empty_cores(res) else res
