from typing import List, Any

from app.types.models import NamedArchive


def find_empty_core(core_data: List[List[Any]]) -> int | None:
    for i, core in enumerate(core_data):
        if not core:
            return i
    return None


def has_empty_cores(core_data: List[List[Any]]) -> bool:
    """
    Check if any of the cores in the given core data are empty.

    Args:
        core_data (List[List[Any]]): The list of core data to check.

    Returns:
        bool: True if any of the cores are empty, False otherwise.
    """
    return not all(core_data)


def remove_empty_cores(core_data: List[List[Any]]) -> List[List[NamedArchive]]:
    """
    Remove any empty cores from the given core data.

    Args:
        core_data (List[List[Any]]): The list of core data to remove empty cores from.

    Returns:
        List[List[NamedArchive]]: The core data without empty cores.
    """
    return [core for core in core_data if core]


def format_duration(duration_seconds: float) -> str:
    """
    Formats the given duration in seconds into a string representation of hours, minutes, and seconds.

    Args:
        duration_seconds (float): The duration in seconds.

    Returns:
        str: The formatted duration in the format "HH:MM:SS".

    Examples:
        >>> format_duration(3661)
        '01:01:01'
        >>> format_duration(7200)
        '02:00:00'
    """
    duration_seconds = int(duration_seconds)

    hours = duration_seconds // 3600
    minutes = (duration_seconds % 3600) // 60
    seconds = duration_seconds % 60

    return f"{hours:02d}:{minutes:02d}:{seconds:02d}"
