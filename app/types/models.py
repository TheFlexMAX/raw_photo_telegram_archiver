from pathlib import WindowsPath
from typing import List, Tuple

File = WindowsPath
Archive = List[File]
NamedArchive = Tuple[Archive, str]
Chunk = List[Archive]
